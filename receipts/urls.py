from django.urls import path
from receipts.views import home_page

urlpatterns = [
    path("", home_page, name="home")
]
